KVM-ting
====

Funktionalitet og krav:
---

* Et fysisk tastatur til mange computere
* Ingen mekanisk switch, computere skal tro tastaturet sidder til hele tiden (dvs: en controller pr. computer)
* Man skal kunne bestemme hvilken computer tastaturet sender til fra tastaturet
* Man skal kunne slutte flere computere til uden særligt besvær, også dynamisk
* Samme hardware til alle controllere, både med og uden fysisk tastatur (med mindre der skal laves eks. en PS/2-udgave)
* Samme software på alle controllere (med mindre der skal laves eks. en PS/2-udgave)
* Ingen konfiguration for at sætte kæden op.

Princip
----

Controlleren som sidder til det fysiske keyboard er den samme som den der sidder til de forskellige computere.

Keyboardcontrollernes UARTs sidder i en kæde. Controller 1s sender til Controller 2, som sender til Controller 3, etc.

Hver controller er altid USB-device og svarer altid pænt når hosten poller med de tastetryk den har liggende.

Tastetryk kan komme to steder fra: matrixscan-rutinen, eller via UART.

Ifht. tastetryk fra matricen kan controlleren kan være i to tilstande: enten sender man direkte til USB, eller også sender man videre. Hvis man sender videre, har man et niveau, der bestemmer hvor mange led man sender videre i kæden. Det sættes kun fra selve tastaturet. Hvis man får et tastetryk på matricen kigger man på sin tilstand:

* Hvis man er direkte, sender man tastetrykker via USB.
* Hvis man er videresender, sender man tastetrykket via UART, mærket med niveau.  

Tastetryk over UART sendes enten over USB eller videre over UART. Hvis man får et tastetryk på UARTen kigger man på niveaumærket:

* Hvis det er 0, sender man tastetrykket via USB.
* Hvis det er større end 0 sender man det via UART, mærket en mindre end det mærke man fik.

UART Protokol
----

USB HID-protokollen centrerer sig omkring datastrukturen en report. Periodisk beder hosten et device om en rapport af hvilken knapper der er trykket ned, om der er sket noget med musen, etc.

TMK er grundlæggende en måde at omdanne resultatet af scanning af en knapmatrix til HID-rapporter.

Vi vil gerne flytte HID-rapporten over UART istedet så den ligger klar på en anden USB host. Det gør vi ved at præfigere rapporten med en byte, der forklarer hvad der kommer efter. Vi kalder kombinationen af en HID report og en beskrivende byte for en pakke.

0b0100xxxx - en boot protocol keyboard report (8 bytes) med niveaumærke xxxx.
0b0101xxxx - en mouse report (5 bytes) med niveaumærke xxxx.
0b01??xxxx - reserveret

Derudover vil vi gerne kunne sende kommandoer gennem kæden når f.eks. den valgte slave skifter.

0b1000xxxx - skift til slave nummer xxxx. hver led sender byten videre, minus 1. 
             hvis man er den der går fra 1 til 0 ved man man er den. 
             hvis man får 0 ved man man ikke er den længere.
